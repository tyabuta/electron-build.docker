electron-builder
================

## ビルド環境最新化

```
npm -i -g npm
npm -g install electron-prebuilt
npm -g install electron-packager
```

## node バージョン

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
command -v nvm
nvm install 6.5
node --version
```



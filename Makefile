
CNAME=electron-builder

ps:
	docker ps

pull:
	docker pull electronuserland/electron-builder
	docker pull electronuserland/electron-builder:wine

run:
	docker run -itd --name=$(CNAME) -v ${PWD}/projects:/project electronuserland/electron-builder:wine

stop:
	docker stop $(CNAME)
	docker rm   $(CNAME)

bash:
	docker exec -it $(CNAME) bash


